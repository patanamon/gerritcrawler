from GerritServices import ChangeListService,ChangeDetailService,PatchDetailService
from settings import *
from Utils.MyLog import logging
import json

def getChangeList(status="ALL"):
	if status == "ALL":
		status_list = ["open","merged","abandoned"]
	else:
		status_list = [status]
	for status in status_list:
		atEnd = False
		nextSortKey = None
		while(not atEnd):
			dat = ChangeListService.allQueryNext(status,nextSortKey)
			if "result" not in dat.keys():
				logging("No Result [MSG] "+str(dat),"ERROR")
				continue
			dat = dat["result"]
			id_file = open(ID_LIST,"a")
			atEnd = dat["atEnd"]
			for change in dat["changes"]:
				id_file.write(str(change["id"]["id"])+"\n")
				nextSortKey = change["sortKey"]
			id_file.close()
			logging("[atEnd] "+str(dat["atEnd"])+" [nextSortKey] "+nextSortKey)

def getChange():
	f = open(ID_LIST)
	id_cnt = int(args.at_line)
	out_review_cnt = id_cnt/LINE_LIMIT
	if out_review_cnt == 0:
		change_file = open(OUT_REVIEWS,"a")
	else:
		change_file = open(OUT_REVIEWS+"."+str(out_review_cnt),"a")
		
	for change_id in f.readlines()[int(args.at_line):]:
		change_id = change_id.rstrip('\n')
		dat = ChangeDetailService.changeDetail(change_id)
		if args.include_diff.lower == "true":
			diffInfo = {}
			if "result" not in dat.keys():
				logging("No Result [MSG] "+str(dat),"ERROR")
				continue
			for d in dat["result"]["currentDetail"]["patches"]:
				if d["key"]["fileName"] == "/COMMIT_MSG":
					continue
				diff = PatchDetailService.patchScript(d["key"]["fileName"],dat["result"]["change"]["changeId"]["id"],dat["result"]["currentPatchSetId"]["patchSetId"])
				diffInfo[d["key"]["fileName"]] = diff
			dat["diffInfo"] = diffInfo
		if id_cnt >= LINE_LIMIT:
			change_file.close()
			out_review_cnt += 1
			change_file = open(OUT_REVIEWS+"."+str(out_review_cnt),"a")
		change_file.write(json.dumps(dat)+"\n")