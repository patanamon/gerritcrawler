from Utils.HttpConnection import JSON_Request
import json
from settings import *
from Utils.MyLog import logging

def patchScript(fileName,changeId,patchSetId):
	params = {"jsonrpc":"2.0","method":"patchScript","params":[{"fileName":fileName,"patchSetId":{"changeId":{"id":changeId},"patchSetId":patchSetId}},None,{"changeId":{"id":changeId},"patchSetId":patchSetId},{"context":10,"expandAllComments":False,"ignoreWhitespace":"N","intralineDifference":True,"lineLength":100,"manualReview":False,"retainHeader":False,"showTabs":True,"showWhitespaceErrors":True,"skipDeleted":False,"skipUncommented":False,"syntaxHighlighting":True,"tabSize":8}],"id":2}
	logging("[Request] "+str(params))
	ret = JSON_Request("PatchDetailService",params)
	logging("[Response]"+ret)
	return json.loads(ret)


def patchScript2(fileName,changeId_a,patchSetId_a,changeId_b,patchSetId_b):
	if changeId_a == 0 or patchSetId_a == 0:
		params = {"jsonrpc":"2.0","method":"patchScript","params":[{"fileName":fileName,"patchSetId":{"changeId":{"id":changeId_b},"patchSetId":patchSetId_b}},None,{"changeId":{"id":changeId_b},"patchSetId":patchSetId_b},{"context":10,"expandAllComments":False,"ignoreWhitespace":"N","intralineDifference":True,"lineLength":100,"manualReview":False,"retainHeader":False,"showLineEndings":True,"showTabs":True,"showWhitespaceErrors":True,"skipDeleted":False,"skipUncommented":False,"syntaxHighlighting":True,"tabSize":8}],"id":1}
	else:	
		params = {"jsonrpc":"2.0","method":"patchScript","params":[{"fileName":fileName,"patchSetId":{"changeId":{"id":changeId_b},"patchSetId":patchSetId_b}},{"changeId":{"id":changeId_a},"patchSetId":patchSetId_a},{"changeId":{"id":changeId_b},"patchSetId":patchSetId_b},{"context":10,"expandAllComments":False,"ignoreWhitespace":"N","intralineDifference":True,"lineLength":100,"manualReview":False,"retainHeader":False,"showLineEndings":True,"showTabs":True,"showWhitespaceErrors":True,"skipDeleted":False,"skipUncommented":False,"syntaxHighlighting":True,"tabSize":8}],"id":1}
	logging("[Request] "+str(params))
	ret = JSON_Request("PatchDetailService",params)
	logging("[Response]"+ret)
	return json.loads(ret)

