from Utils.HttpConnection import JSON_Request
import json
from settings import *
from Utils.MyLog import logging

# Request a list of reviews corresponding to status
def allQueryNext(status,nextSortKey=None,listSize=25):
	if nextSortKey is None:
		nextSortKey = "z"
	params = {"jsonrpc":"2.0","method":"allQueryNext","params":["status:"+status,nextSortKey,listSize],"id":1}
	logging("[Request] "+str(params))
	ret = JSON_Request("ChangeListService",params)
	logging("[Response]"+ret)
	return json.loads(ret)