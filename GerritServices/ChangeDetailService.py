from Utils.HttpConnection import JSON_Request
import json
from settings import *
from Utils.MyLog import logging


# Request current information of a review
def changeDetail(changeId):
	params = {"jsonrpc":"2.0","method":"changeDetail","params":[{"id":changeId}],"id":1}
	logging("[Request] "+str(params))
	ret = JSON_Request("ChangeDetailService",params)
	logging("[Response]"+ret)
	return json.loads(ret)

#Request patch info
def patchSetDetail2(changeId_a,patchSetId_a,changeId_b,patchSetId_b):
	if patchSetId_a == 0:
		params = {"jsonrpc":"2.0","method":"patchSetDetail2","params":[None,{"changeId":{"id":changeId_b},"patchSetId":patchSetId_b},None],"id":1}
	else:
		params = {"jsonrpc":"2.0","method":"patchSetDetail2","params":[{"changeId":{"id":changeId_a},"patchSetId":patchSetId_a},{"changeId":{"id":changeId_b},"patchSetId":patchSetId_b},{"context":10,"expandAllComments":False,"ignoreWhitespace":"N","intralineDifference":True,"lineLength":100,"manualReview":False,"retainHeader":False,"showLineEndings":True,"showTabs":True,"showWhitespaceErrors":True,"skipDeleted":False,"skipUncommented":False,"syntaxHighlighting":True,"tabSize":8}],"id":1}
	logging("[Request] "+str(params))
	ret = JSON_Request("ChangeDetailService",params)
	logging("[Response]"+ret)
	return json.loads(ret)
