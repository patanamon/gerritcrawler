#This is for retrieving diff chunks and inline comments
from GerritServices import PatchDetailService,ChangeDetailService
from settings import *
import json
from time import sleep

f = open(ID_LIST)
fpatch = open("out/patchset_"+ID_LIST+".csv","a")
fpatch.write("ChangeId,PatchSetId,AuthorId,UploadDate,FileName,Churn\n")
fpatch.close()

for change_id in f.readlines()[int(args.at_line):]:
	(dum,change_id,numPatch) = change_id.rstrip('\n').split(",")
	change_id = int(change_id)
	numPatch = int(numPatch)
	curr_num_file = 0
	parent = None
	for i in range(1,numPatch+1):
		dat = ChangeDetailService.patchSetDetail2(change_id,i-1,change_id,i)
		if dat["result"]["info"]["author"].has_key("accountId"):
			author_id = dat["result"]["info"]["author"]["accountId"]["id"]
		else:
			author_id = 0

		OwnerId = str(author_id)
		UploadDate = str(dat["result"]["patchSet"]["createdOn"])

		if len(dat["result"]["info"]["parents"]) > 0:
			if parent is None:
				parent = dat["result"]["info"]["parents"][0]["id"]["id"]
			elif parent != dat["result"]["info"]["parents"][0]["id"]["id"]:
				parent = dat["result"]["info"]["parents"][0]["id"]["id"]
				continue
		else:
			continue

		# if  len(dat["result"]["patches"]) > 50:
		# 	fexeed= open("out/exeed_"+ID_LIST+".csv","a")
		# 	fexeed.write(str(change_id)+","+str(i)+"\n")
		# 	fexeed.close()
		# 	continue

		for d in dat["result"]["patches"]:
			if d["nbrComments"] > 0:
				diff = PatchDetailService.patchScript2(d["key"]["fileName"],change_id,i-1,change_id,i)
				fcomment = open("out/comment_"+ID_LIST+".csv","a")
				fcomment.write(json.dumps({"reviewId":change_id,"patchSetId":i,"file":d["key"]["fileName"],"inlineComment":diff["result"]["comments"]["b"]})+"\n")
				fcomment.close()
			if d["key"]["fileName"] != "/COMMIT_MSG":
				#diff = PatchDetailService.patchScript2(d["key"]["fileName"],change_id,i-1,change_id,i)
				fpatch = open("out/patchset_"+ID_LIST+".csv","a")
				fpatch.write(str(change_id)+","+str(i)+","+OwnerId+","+UploadDate+","+d["key"]["fileName"]+","+str(d["insertions"]+d["deletions"])+"\n")
				fpatch.close()
				fpatch.write(str(change_id)+","+str(i)+","+OwnerId+","+UploadDate+","+d["key"]["fileName"]+","+diff["result"]["changeType"]+","+str(diff["result"]["a"]["size"])+","+str(diff["result"]["b"]["size"])+","+str(Churn)+"\n")
		sleep(5)