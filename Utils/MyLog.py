import inspect
from settings import *

def logging(msg,type="INFO"):
	curframe = inspect.currentframe()
	calframe = inspect.getouterframes(curframe, 2)
	type = type.upper()
	if FULL_LOG:
		caller_name = calframe[1][1]+" "
	else:
		caller_name = ""
	caller_name += calframe[1][3]
	if LOG_CONSOLE:
		print type,":",caller_name,msg
		print 

	if type == "ERROR":
		f = open(ERR_FILE,"a")
		f.write(type+" : "+str(caller_name)+" [MSG] "+str(msg)+"\n")
		f.close()