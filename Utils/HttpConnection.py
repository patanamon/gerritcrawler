#USING JSON-RPC
import urllib,urllib2,json,httplib,sys
from settings import *
from time import sleep
from Utils.MyLog import logging

def JSON_Request(service,params):
	data = json.dumps(params)
	headers = {
	"Connection": "keep-alive",
	"Content-Length":len(data),
	"Accept": "application/json,application/json,application/jsonrequest",
	"Origin": HTTPS_URL,
	"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.76 Safari/537.36",
	"Content-Type": "application/json; charset=UTF-8",
	"Referer": HTTPS_URL+"/"+URL_r,
	"Accept-Language": "en-US;q=0.8,en;q=0.6"
	}
	if args.secure == "yes":
		conn=httplib.HTTPSConnection(URL)
	else:
		conn=httplib.HTTPConnection(URL)
	attempt = 0
	while(attempt < 2):
		if args.method == "RPC":
			conn.request("POST","/"+URL_r+"gerrit/rpc/"+service,data,headers)
		elif args.method == "API":
			conn.request("POST","/"+URL_r+"gerrit_ui/rpc/"+service,data,headers)
		try:
			res = conn.getresponse()
		except:
			logging("Unexpected Error: "+str(sys.exc_info()[0]),"ERROR")
			continue
		if res.status == 200:
			logging("STATUS: "+str(res.status)+", REASON: "+str(res.reason))
			sleep(DELAY)
			break
		else:
			logging("STATUS: "+str(res.status)+", REASON: "+str(res.reason)+", Attempt to request")
			sleep(DELAY*attempt*10)
		attempt += 1
	return res.read()
	
def API_Request(uri):
	headers = {
		"Accept":"application/json",
		"Accept-Encoding":"text/plain",
		"Accept-Language":"en-US;q=0.8,en;q=0.6",
		"Connection":"keep-alive",
		"Host": URL,
		"Referer": HTTPS_URL,
		"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36"
	}
	if args.secure == "yes":
		conn=httplib.HTTPSConnection(URL)
	else:
		conn=httplib.HTTPConnection(URL)
	attempt = 0
	while(attempt < 10):
		conn.request("GET","/"+uri,headers=headers)
		try:
			res = conn.getresponse()
		except:
			logging("Unexpected Error: "+str(sys.exc_info()[0]),"ERROR")
			continue
		if res.status == 200:
			logging("STATUS: "+str(res.status)+", REASON: "+str(res.reason))
			sleep(DELAY)
			break
		else:
			logging("STATUS: "+str(res.status)+", REASON: "+str(res.reason)+", Attempt to request")
			sleep(DELAY*attempt*10)
		attempt += 1
	if attempt < 10:
		return res.read()
	else:
		logging("CANNOT Load: "+uri,"ERROR")
		return None