from settings import *
from Utils.HttpConnection import API_Request
from Utils.MyLog import logging
import json


# Request a list of reviews corresponding to status
def queryChanges(status,limit,cumlative):
	uri = URL_r+"changes/?q=status:"+status+"&n="+str(limit)+"&O=1"
	if cumlative > 0:
		uri += "&S="+str(cumlative)
	logging("[REQUEST] "+uri)
	ret = API_Request(uri)
	if ret is not None:
		logging("[RESPONSE] "+ret)
		return json.loads(ret.split("\n")[1])
	else:
		return None


def queryChanges_sortKey(status,limit,sortKey=None):
	uri = URL_r+"changes/?q=status:"+status+"&n="+str(limit)+"&O=1"
	if sortKey is not None:
		uri += "&N="+sortKey
	logging("[REQUEST] "+uri)
	ret = API_Request(uri)
	
	if ret is not None:
		logging("[RESPONSE] "+ret)
		return json.loads(ret.split("\n")[1])
	else:
		return None

def getFullDetail(changeId):
	uri = URL_r+"changes/?q="+str(changeId)+"&o=ALL_REVISIONS&o=ALL_COMMITS&o=ALL_FILES&o=LABELS&o=DETAILED_LABELS&o=MESSAGES&o=DETAILED_ACCOUNTS"
	logging("[REQUEST] "+uri)
	ret = API_Request(uri)
	
	if ret is not None:
		logging("[RESPONSE] "+ret)
		return json.loads(ret.split("\n")[1])
	else:
		return None

def getCurrentDetail(changeId):
	uri = URL_r+"changes/?q="+str(changeId)+"&o=CURRENT_REVISION&o=ALL_COMMITS&o=CURRENT_FILES&o=LABELS&o=DETAILED_LABELS&o=MESSAGES&o=DETAILED_ACCOUNTS"
	logging("[REQUEST] "+uri)
	ret = API_Request(uri)
	
	if ret is not None:
		logging("[RESPONSE] "+ret)
		return json.loads(ret.split("\n")[1])
	else:
		return None