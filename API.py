from settings import *
from Utils.MyLog import logging
from GerritAPIs import ChangeEndPoints
from GerritServices import PatchDetailService
import json

def getChangeList(status="ALL"):
	if status == "ALL":
		status_list = ["open","merged","abandoned"]
	else:
		status_list = [status]
	id_file = open(ID_LIST,"a")
	for status in status_list:
		has_more = True
		cum = 0
		n = 25
		last_sortKey = None
		while has_more:
			if args.query_by == "sortkey":
				print "sortkey"
				dat = ChangeEndPoints.queryChanges_sortKey(status,n,last_sortKey)
			else:
				dat = ChangeEndPoints.queryChanges(status,n,cum)
			if len(dat) == 0:
				break
			for d in dat:
				id_file.write(str(d["_number"])+"\n")
				if "_more_changes" in d.keys():
					has_more = d["_more_changes"]
					last_sortKey = d["_sortkey"]
				else:
					has_more = False
			cum += n
	id_file.close()


def getChange():
	f = open(ID_LIST)
	id_cnt = int(args.at_line)
	#out_review_cnt = id_cnt/LINE_LIMIT
	#if out_review_cnt == 0:
	change_file = open(OUT_REVIEWS,"a")
	#else:
	#change_file = open(OUT_REVIEWS+"."+str(out_review_cnt),"a")
		
	for change_id in f.readlines()[int(args.at_line):]:
		id_cnt += 1
		change_id = change_id.rstrip('\n')
		dat = ChangeEndPoints.getCurrentDetail(change_id)
		if dat is None:
			logging("SKIP REVIEW: "+str(change_id))
			continue
		dat = dat[0]
		if args.include_diff.lower() == "true":
			diffInfo = {}
			if "current_revision" not in dat.keys() :
				logging(str(dat["_number"])+":No Current Revisions, SKIP", "ERROR")
				continue
			for filename in dat["revisions"][dat["current_revision"]]["files"].keys():
				if filename == "/COMMIT_MSG":
					continue
				diff = PatchDetailService.patchScript(filename,dat["_number"],dat["revisions"][dat["current_revision"]]["_number"])
				diffInfo[filename] = diff
			dat["diffInfo"] = diffInfo
		#if id_cnt >= LINE_LIMIT:
		#	change_file.close()
		#	out_review_cnt += 1
		#	change_file = open(OUT_REVIEWS+"."+str(out_review_cnt),"a")
		change_file.write(json.dumps(dat)+"\n")