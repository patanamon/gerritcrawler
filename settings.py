from datetime import date
import os
import argparse

###### Setting Parameters HERE ##########
#Download Delay (Do not less than 1)
DELAY = 3

LINE_LIMIT = 20000

############# OUTPUT CONFIG ###########
# Change ID List file name
FILE_ID_LIST = "ids.txt"
# Current Detail of Review
FILE_REVIEWS = "reviews.json"
# Result Directory
RES_DIR = "results"

########### LOG CONFIG #################
#Show download status on console
LOG_CONSOLE = True
#Show full log
FULL_LOG = False

ERROR_LOG =  "errors.log"
########################################


parser = argparse.ArgumentParser(usage='%(prog)s [options]',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--url", help="gerrit server's url (do not put https://)",required=True)
parser.add_argument("--method", help="Method for download",default="API")
parser.add_argument("--id-list", help="A list of ids for downloading",default=None)
parser.add_argument("--include-diff", help="Download diff file",default="True")
parser.add_argument("--at-line",help="Line number in ID list file that is started to download",default=0)
parser.add_argument("--at-patch",help="Patch number that is started to download (For GerritGetDiffChunk.py)",default=1)
parser.add_argument("--query-by",help="For API, IDs List can be download by review cummulative (cummulative) or using sortkey (sortkey)",default="sortkey")
parser.add_argument("--secure",help="http connection type secure or not: https = yes, http = no (default = yes)",default="yes")


args = parser.parse_args()
URL = args.url.split("/")
if len(URL)  == 2:
	URL_r = URL[1]+"/"
	URL = URL[0]
elif len(URL) == 1:
	URL = URL[0]
	URL_r = ""


# URL = "android-review.googlesource.com"
# URL = "review.openstack.org"
# URL = "codereview.qt-project.org"
HTTPS_URL = "https://"+URL

#Set output files
OUT_DIR = RES_DIR+"/"+URL
if not os.path.exists(OUT_DIR):
    os.makedirs(OUT_DIR)
if args.id_list is not None:
	ID_LIST = args.id_list
	DOWNLOAD_ID_LIST = False
else:
	ID_LIST = OUT_DIR+"/"+FILE_ID_LIST
	DOWNLOAD_ID_LIST = True
OUT_REVIEWS = OUT_DIR+"/"+FILE_REVIEWS
ERR_FILE = OUT_DIR+"/"+ERROR_LOG
