Gerrit Crawler (Python)
================================
A tool for downloading review data sets from Gerrit system

Python Version
================================
Tested on Python 2.7.5

Supported Gerrit Version
===============================
* Less than 2.4 using RPC method
* More than 2.8 using API method

How To Use
================================
	python GerritCrawler.py --url review.openstack.org
	python GerritCrawler.py --url review.openstack.org --method RPC 
	python GerritCrawler.py --url review.openstack.org --id-list ID_LIST_FILE_PATH  %In case you already have a list of id
	python GerritCrawler.py --url review.openstack.org --id-list ID_LIST_FILE_PATH --at-line NUMBER %From the list, specific line to begin. 
																									If --id-list is not specified, it will use the one that is downloaded.

Stand alone scripts
---------------------
* GerritGetDiffChunk - For retrieving diff chunks, patch size and inline comments


	python GerritGetDiffChunk.py  --url codereview.qt-project.org --id-list ID_LIST_FILE_PATH
  
 
The ID_LIST_FILE_PATH must be formatted in 


	rowID(Dummy),ReviewId,NumberOfPatches


Requires:  GerritServices/PatchDetailService, GerritServices/ChangeDetailService


Command Line Options
================================
	optional arguments:
	-h, --help            		show this help message and exit
	--url URL             		[Required] gerrit server's url (do not put https://) (default: None)
	--method METHOD       		Method for download API or RPC (default: API)
	--id-list ID_LIST     		A list of ids for downloading (default: None)
	--include-diff INCLUDE_DIFF	IDs list for download (default: True)
	--at-line AT_LINE     		Line number in ID list file that is started to download (default: 0)
	--query-by QUERY_BY   		For API, IDs List can be download by review cummulative (cummulative) or using sortkey (sortkey) (default: sortkey)
                        
Configuration
================================
In settings.py, you can config some variables

	###### Setting Parameters HERE ##########
	#Download Delay (Do not less than 1)
	DELAY = 5

	############# OUTPUT CONFIG ###########
	# Change ID List file name
	FILE_ID_LIST = "ids.txt"
	# Current Detail of Review
	FILE_REVIEWS = "reviews.json"
	# Result Directory
	RES_DIR = "results"

	########### LOG CONFIG #################
	#Show download status on console
	LOG_CONSOLE = True
	#Show full log
	FULL_LOG = False

	ERROR_LOG =  "errors.log"
	########################################
                        
